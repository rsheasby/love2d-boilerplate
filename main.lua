-- include all necessary libraries
-- note: you will need to clone these before this boilerplate will work.
Camera = require 'lib.hump.camera'
Class = require 'lib.classic.classic'
Deep = require 'lib.deep.deep'

local camera
GameObjs = {}

function love.load()
    -- add all the game object classes
    require 'objs/objs'
    -- instantiate game objects
    GameObjs.background = Background()
    GameObjs.b1 = Ball()

    --create camera
    camera = Camera.new()
end

function love.update(dt)
    -- update all game objects
    for name, obj in pairs(GameObjs) do obj:update(dt) end
end

function love.draw()
    camera:lockPosition(GameObjs.b1.x, GameObjs.b1.y)
    camera:attach()
    -- draw all game objects within the camera's view
    for name, obj in pairs(GameObjs) do obj:draw() end
    Deep.execute()
    camera:detach()
end
