Ball = GameObj:extend()

function Ball:new()
    self.x = 100
    self.y = 100
    self.z = 2
    self.color = {r = 1, g = 0.8, b = 0.8}
    self.speed = 500
    self.xv, self.yv = 0, 0
end

function Ball:draw()
    Deep.queue(self.z, function()
        love.graphics.setColor(self.color.r, self.color.g, self.color.b)
        love.graphics.circle("fill", self.x, self.y, 15)
    end)
end

function Ball:update(dt)
    local a1, a2 = 0, 0
    if love.keyboard.isDown('left') then a2 = a2 + 0.15 end
    if love.keyboard.isDown('down') then a1 = a1 + 0.15 end
    if love.keyboard.isDown('right') then a2 = a2 - 0.15 end
    if love.keyboard.isDown('up') then a1 = a1 - 0.15 end
    self.xv = 0.98 * self.xv + -a2 * self.speed
    self.yv = 0.98 * self.yv + a1 * self.speed
    self.x = self.x + self.xv * dt
    self.y = self.y + self.yv * dt
    if self.x < 15 then self.x, self.xv = 15, 0 end
    if self.x > 1009 then self.x, self.xv = 1009, 0 end
    if self.y < 15 then self.y, self.yv = 15, 0 end
    if self.y > 753 then self.y, self.yv = 753, 0 end
end
