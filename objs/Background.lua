Background = GameObj:extend()

function Background:draw()
    Deep.queue(1, function()
        love.graphics.setColor(0.7, 0.7, 0.9)
        love.graphics.rectangle('line', 0, 0, 1024, 768)
    end)
end