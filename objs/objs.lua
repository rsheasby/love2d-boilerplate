-- create boilerplate for game objects
GameObj = Class:extend()

function GameObj:new()
    -- callback for when the object is instantiated
end

function GameObj:update(dt)
    -- callback for whenever the game updates
end

function GameObj:draw()
    -- callback for whenever the game draws
end

require 'objs.Background'
require 'objs.Ball'
-- put other game object class files here
